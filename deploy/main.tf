terraform {
  backend "s3" {
    bucket  = "recipe-app-api-devops-tfstatemuzammil"
    key     = "recipe-app.tfstate"
    region  = "us-east-1"
    encrypt = true
    # dynamodb_table = "recipe-app-api-devops-tfstate-lock"
  }
}

provider "aws" {
  region = "us-east-1"
}

locals {
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}
